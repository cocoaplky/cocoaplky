FROM dasbastard/mirror:dev

WORKDIR /usr/src/app
RUN chmod 777 /usr/src/app

RUN git clone https://gitlab.com/cocoaplky/cocoaplky.git /usr/src/app
RUN mkdir /usr/src/app/dl
RUN chmod 777 /usr/src/app/dl
COPY extract /usr/local/bin
RUN chmod +x /usr/local/bin/extract
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
COPY . .
COPY netrc /root/.netrc
RUN chmod +x aria.sh

CMD ["bash","start.sh"]



